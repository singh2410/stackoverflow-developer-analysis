#!/usr/bin/env python
# coding: utf-8

# # Stack Overflow Developer Survey Analysis using Survey Data of 2020
# #By- Aarush Kumar
# #Dated: August 30,2021

# In[1]:


from IPython.display import Image
Image(url='https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4e990e20126391.58b02ab778b00.jpg')


# In[2]:


import pandas as pd


# In[3]:


survey_raw_df = pd.read_csv('/home/aarush100616/Downloads/Projects/Stack Overflow Developer Survey /survey_results_public.csv')


# In[4]:


survey_raw_df


# In[5]:


survey_raw_df.columns


# In[6]:


schema_fname =  '/home/aarush100616/Downloads/Projects/Stack Overflow Developer Survey /survey_results_schema.csv'
schema_df = pd.read_csv(schema_fname, index_col='Column')
schema_df


# In[7]:


schema_raw = schema_df.QuestionText


# In[8]:


schema_raw


# In[9]:


schema_raw['YearsCodePro']


# In[10]:


schema_raw['ConvertedComp']


# In[11]:


schema_raw['WorkWeekHrs']


# In[12]:


selected_columns = [
    # Demographics
    'Country',
    'Age',
    'Gender',
    'EdLevel',
    'UndergradMajor',
    # Programming experience
    'Hobbyist',
    'Age1stCode',
    'YearsCode',
    'YearsCodePro',
    'LanguageWorkedWith',
    'LanguageDesireNextYear',
    'NEWLearn',
    'NEWStuck',
    # Employment
    'Employment',
    'DevType',
    'WorkWeekHrs',
    'JobSat',
    'JobFactors',
    'NEWOvertime',
    'NEWEdImpt',
    'ConvertedComp'
]


# In[13]:


len(selected_columns)


# In[14]:


survey_df = survey_raw_df[selected_columns].copy()


# In[15]:


schema = schema_raw[selected_columns]


# In[16]:


survey_df.shape


# In[17]:


survey_df.info()


# In[18]:


survey_df['Age1stCode'].unique()


# In[19]:


survey_df['YearsCode'].unique()


# In[20]:


survey_df['YearsCodePro'].unique()


# In[21]:


survey_df['Age1stCode'] = pd.to_numeric(survey_df.Age1stCode, errors='coerce')
survey_df['YearsCode'] = pd.to_numeric(survey_df.YearsCode, errors='coerce')
survey_df['YearsCodePro'] = pd.to_numeric(survey_df.YearsCodePro, errors='coerce')


# In[22]:


survey_df.describe()


# In[23]:


survey_df.drop(survey_df[survey_df.Age < 10].index, inplace=True)
survey_df.drop(survey_df[survey_df.Age > 100].index, inplace=True)


# In[24]:


survey_df.drop(survey_df[survey_df.WorkWeekHrs > 140].index, inplace=True)


# In[25]:


survey_df['Gender'].value_counts()


# In[26]:


import numpy as np


# In[27]:


survey_df.where(~(survey_df.Gender.str.contains(';', na=False)), np.nan, inplace=True)


# In[28]:


survey_df.sample(10)


# In[29]:


import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
sns.set_style('darkgrid')
matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['figure.figsize'] = (9, 5)
matplotlib.rcParams['figure.facecolor'] = '#00000000'


# In[30]:


schema.Country


# In[31]:


survey_df.Country.nunique()


# In[32]:


top_countries = survey_df.Country.value_counts().head(15)
top_countries


# In[33]:


plt.figure(figsize=(12,6))
plt.xticks(rotation=75)
plt.title(schema.Country)
sns.barplot(x=top_countries.index, y=top_countries);


# In[34]:


import folium 


# In[35]:


countries_geojson = 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'


# In[36]:


country_counts = survey_df.Country.value_counts()
country_counts_df = pd.DataFrame({ 'Country': country_counts.index, 'Count': country_counts.values})
country_counts_df


# In[37]:


country_counts_df.at[0, 'Country'] = 'United States of America'
country_counts_df.at[12, 'Country'] = 'Russia'


# In[38]:


m = folium.Map(location=[30, 0], zoom_start=2)

folium.Choropleth(
    geo_data=countries_geojson,
    data=country_counts_df,
    columns=["Country", "Count"],
    key_on="feature.properties.name",
    threshold_scale=[1, 30, 100, 300, 1_000, 3_000, 10_000, 13_000],
    fill_color="OrRd",
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name="Respondents",
).add_to(m)
m


# In[39]:


import plotly.express as px


# In[40]:


px.histogram(survey_df, x="Age", marginal="box", title='Age of Respondents')


# In[41]:


schema.Gender


# In[42]:


gender_counts = survey_df.Gender.value_counts()
gender_counts


# In[43]:


plt.figure(figsize=(12,6))
plt.title(schema.Gender)
plt.pie(gender_counts, labels=gender_counts.index, autopct='%1.1f%%', startangle=180);


# In[44]:


sns.countplot(y=survey_df.EdLevel)
plt.xticks(rotation=75);
plt.title(schema['EdLevel'])
plt.ylabel(None);


# In[45]:


schema.UndergradMajor


# In[46]:


undergrad_pct = survey_df.UndergradMajor.value_counts() * 100 / survey_df.UndergradMajor.count()

sns.barplot(x=undergrad_pct, y=undergrad_pct.index)

plt.title(schema.UndergradMajor)
plt.ylabel(None);
plt.xlabel('Percentage');


# In[47]:


schema.Employment


# In[48]:


(survey_df.Employment.value_counts(normalize=True, ascending=True)*100).plot(kind='barh', color='g')
plt.title(schema.Employment)
plt.xlabel('Percentage');


# In[49]:


schema.DevType


# In[50]:


survey_df.DevType.value_counts()


# In[51]:


def split_multicolumn(col_series):
    result_df = col_series.to_frame()
    options = []
    # Iterate over the column
    for idx, value  in col_series[col_series.notnull()].iteritems():
        # Break each value into list of options
        for option in value.split(';'):
            # Add the option as a column to result
            if not option in result_df.columns:
                options.append(option)
                result_df[option] = False
            # Mark the value in the option column as True
            result_df.at[idx, option] = True
    return result_df[options]


# In[52]:


dev_type_df = split_multicolumn(survey_df.DevType)


# In[53]:


dev_type_df 


# In[54]:


dev_type_totals = dev_type_df.sum().sort_values(ascending=False)
dev_type_totals


# In[55]:


from wordcloud import WordCloud


# In[56]:


survey_df.DevType


# In[57]:


words = ' '.join((job for job in survey_df.DevType.dropna().str.replace(';', ' ').str.replace(',', ' ')))

wc = WordCloud(collocation_threshold=int(1e6), width=800, height=400, background_color='white').generate(words)

plt.figure(figsize=(16,8))
plt.axis("off")
plt.grid(False)
plt.imshow(wc);


# In[58]:


survey_df.columns


# In[59]:


usa_df = survey_df[survey_df.Country.isin(['United States', 'India'])]
px.scatter(usa_df, x='WorkWeekHrs', y='ConvertedComp', color='Country')


# In[60]:


px.histogram(survey_df, x="WorkWeekHrs", title='Hours of Work Per Week', nbins=40)


# In[61]:


px.histogram(survey_df, x="ConvertedComp", marginal="box", title='Compensation of Respondents')


# In[62]:


survey_df.LanguageWorkedWith


# In[63]:


languages_worked_df = split_multicolumn(survey_df.LanguageWorkedWith)


# In[64]:


languages_worked_df 


# In[65]:


languages_worked_percentages = languages_worked_df.mean().sort_values(ascending=False) * 100
languages_worked_percentages


# In[66]:


plt.figure(figsize=(12, 12))
sns.barplot(x=languages_worked_percentages, y=languages_worked_percentages.index)
plt.title("Languages used in the past year");
plt.xlabel('count');


# In[67]:


languages_interested_df = split_multicolumn(survey_df.LanguageDesireNextYear)
languages_interested_percentages = languages_interested_df.mean().sort_values(ascending=False) * 100
languages_interested_percentages


# In[68]:


plt.figure(figsize=(12, 12))
sns.barplot(x=languages_interested_percentages, y=languages_interested_percentages.index)
plt.title("Languages people are intersted in learning over the next year");
plt.xlabel('count');


# In[69]:


languages_loved_df = languages_worked_df & languages_interested_df


# In[70]:


languages_loved_percentages = (languages_loved_df.sum() * 100/ languages_worked_df.sum()).sort_values(ascending=False)


# In[71]:


plt.figure(figsize=(12, 12))
sns.barplot(x=languages_loved_percentages, y=languages_loved_percentages.index)
plt.title("Most loved languages");
plt.xlabel('count');


# In[72]:


countries_df = survey_df.groupby('Country')[['WorkWeekHrs']].mean().sort_values('WorkWeekHrs', ascending=False)


# In[73]:


high_response_countries_df = countries_df.loc[survey_df.Country.value_counts() > 250].head(15)
high_response_countries_df 


# In[74]:


schema.YearsCodePro


# In[75]:


sns.scatterplot(x='Age', y='YearsCodePro', hue='Hobbyist', data=survey_df)
plt.xlabel("Age")
plt.ylabel("Years of professional coding experience");


# In[76]:


plt.title(schema.Age1stCode)
sns.histplot(x=survey_df.Age1stCode, bins=30, kde=True);

